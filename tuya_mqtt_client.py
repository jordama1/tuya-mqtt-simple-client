import paho.mqtt.client as mqtt
import json
import hmac
import hashlib
import binascii
import base64
import time
import json
import ssl

#insert your as a string device ID here
device_ID = "device ID"
#insert your secret device ID here
device_secret_ID = "device secret ID"
#insert path to your tls certificate in .pem format here, obtained from tuya's website: https://developer.tuya.com/en/docs/iot/MQTT-protocol?id=Kb65nphxrj8f1
cert_path = "/filepath"

#start the client
client = mqtt.Client(client_id="tuyalink_"+device_ID)
#10-digit unix timestamp
timestamp = int(time.time())

#callback that triggers on conection, subscribe to topics here, so the client resubscribes to topics upon reconnection
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("tylink/"+device_ID+"/thing/model/get_response", qos=1)
    client.publish("tylink/"+device_ID+"/thing/model/get", qos=1)
    client.subscribe("tylink/"+device_ID+"/thing/property/set", qos=1)

#callback for handling incoming messages
def on_message(client, userdata, msg):
    ret = json.loads(msg.payload)
    switch_1 = ret['data']['switch_1']
    value_1 = ret['data']['value_1']
    string_1 = ret['data']['string_1']
    print(ret['data']['switch_1'])
    print("value_1: "+str(value_1))
    print("string_1: "+string_1)

    #print(msg.topic+" "+str(msg.payload))

#publishes device properties to report topic
def report_properties(client, timestamp, properties):
    ret_pub = client.publish(str('tylink/'+device_ID+'/thing/property/report'),payload = properties, qos = 1)
    #print(properties)

#callback for logs    
def on_log(client, userdata, level, buf):
    #uncomment next line to view logs
    #print (buf)
    pass

#define properties of the device
switch_1 = True
value_1 = 1
string_1 = "Hello world!"

#content string - deviceId=${DeviceID},timestamp=${10-digit current timestamp},secureMode=1,accessType=1
content_string = "deviceId="+device_ID+",timestamp="+str(timestamp)+",secureMode=1,accessType=1"

#sha256 hash from content_string with device_secret_ID as a key
digest = hmac.new(device_secret_ID.encode('UTF-8'), content_string.encode('UTF-8'), hashlib.sha256)

signature = digest.hexdigest()
#print(signature)

#connect callbacks
client.on_connect = on_connect
client.on_message = on_message
client.on_log = on_log

#setup username and password
#username string - ${DeviceID}|signMethod=hmacSha256,timestamp=${10-digit current timestamp},secureMode=1,accessType=1;
client.username_pw_set(device_ID+"|signMethod=hmacSha256,timestamp="+str(timestamp)+",secureMode=1,accessType=1", password=signature)

#enable TLS, set certificate
client.tls_set(ca_certs=cert_path, tls_version=ssl.PROTOCOL_TLSv1_2)

#connect client to tuya server
client.connect('m1.tuyacn.com', port=8883, keepalive=60)

#create json to with properties to report
properties = json.dumps({"msgId":"1","time":timestamp,"data":{"switch_1":{"value":switch_1,"time":timestamp},"value_1":{"value":value_1,"time":timestamp},"string_1":{"value":string_1,"time":timestamp}}}, indent=4)
#publish device properties to report topic
report_properties(client, timestamp, properties)

#loop client forever, alternatively can use client.loop_start() and client.loop_stop()
client.loop_forever()
# Tuya simple MQTT client

This code is a demonstrational example Tuya MQTT client, which implements basic functionality for Tuya MQTT communication, such as reporting and receiving device properties from Tuya cloud. Device properties for this device are switch_1 (bool), value_1 (int) and string_1 (string)

## Requirements

As this program is based on paho-mqtt library, you need to install paho-mqtt library using the package manager [pip](https://pip.pypa.io/en/stable/).

```bash
pip install paho-mqtt
```

## Contributing

Pull requests are welcome.
